# -*- coding: utf-8 -*-
"""
Created on Tue Feb 19 09:45:07 2019

@author: longv
"""
#%% import
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np

#%% mise en forme des données extract_cass
data = pd.read_csv("extract_cass.csv")
data['date'].unique() #donne la liste des dates distinctes
data['date'].value_counts() #Nb d'occurences pour chaque valeur
missing_val_count_by_column = (data.isnull().sum())
print(missing_val_count_by_column[missing_val_count_by_column > 0])

#%% mise en forme des donnees extract_sql
sql = pd.read_csv("extract_sql.csv")
sql['date_packing_logistique'].value_counts() 
columns = ["id_produit", "id_isbn", "etat", "statut", "date_insertion", "date_mel",
           "date_vendu","prix_ttc","fdp_ttc"]
sql2 = sql[columns] #dataframe avec les colonnes qui nous intéresse
sql2['statut']=sql2['statut'].map({999 : 1, 5 : 0, 2 : 0, 1 : 0, 404 : 0}) # 1 pour les livres vendus 0 pour les autres

sql2sold = sql2.loc[sql2['statut'] == 1] #tous les livres qui ont été vendus
sql2sold = sql2sold.drop(['statut'], axis =1) #abandon de la colonne aves tous les 999 (axis = 1 pour une colonne 0 pour une ligne)


sql2sold['date_insertion'] = pd.to_datetime(sql2sold.date_insertion)
sql2sold['date_mel'].value_counts()
sql2sold = sql2sold.loc[sql2sold['date_mel'] != '0000-00-00'] #on prend que les données ou la date n'est pas 000
sql2sold = sql2sold.loc[sql2sold['date_vendu'] != '0000-00-00']
sql2sold['date_mel'] = pd.to_datetime(sql2sold.date_mel) #conversion en date
sql2sold['date_vendu'] = pd.to_datetime(sql2sold.date_vendu)
days = (sql2sold["date_vendu"] - sql2sold["date_mel"])


sql2sold["prix_tot"] = (sql2sold["prix_ttc"] + sql2sold["fdp_ttc"]) #création d'une colonne pour le prix total
sql2sold["days"] = (sql2sold["date_vendu"] - sql2sold["date_mel"]) #Création d'une nouvelle colonne
sql2sold['days'] = pd.to_numeric(sql2sold.days)
missing_val_count_by_column = (sql2sold.isnull().sum())
print(missing_val_count_by_column[missing_val_count_by_column > 0])
sql2sold['days'].value_counts()

#%% plot avec scatter prix en fonction du délai de vente et états
sql2sold['days'] = pd.to_numeric(sql2sold.days) # je convertis days en float
sql2sold['days'] = sql2sold['days'] /(864*10**11) # la conversion introduit un facteur 864 10^11
sql3 = sql2sold.loc[sql2['id_isbn'] == 7118] 


#%% Traitement des corrélations pour un même numéro ISBN

columns4 = ["etat", "statut","prix_ttc", "fdp_ttc", "date_mel","date_vendu" ]
sql4 = sql2[columns4]

sql4 = sql4.loc[sql4['date_mel'] != '0000-00-00'] #on prend que les données ou la date n'est pas 000
sql4 = sql4.loc[sql4['date_vendu'] != '0000-00-00']
sql4['date_mel'] = pd.to_datetime(sql4.date_mel) #conversion en date
sql4['date_vendu'] = pd.to_datetime(sql4.date_vendu)
sql4["days"] = (sql4["date_vendu"] - sql4["date_mel"]) #Création d'une nouvelle colonne
sql4['days'] = pd.to_numeric(sql4.days)
missing_val_count_by_column = (sql4.isnull().sum())
print(missing_val_count_by_column[missing_val_count_by_column > 0])
sql4['days'].value_counts()

## plot avec scatter prix en fonction du délai de vente et états
sql4['days'] = pd.to_numeric(sql4.days) # je convertis days en float

sql4["prix_tot"] = (sql4["prix_ttc"] + sql4["fdp_ttc"])

del sql4['fdp_ttc']
del sql4['prix_ttc']
del sql4['date_mel']
del sql4['date_vendu']
#sql4['statut']= sql4['statut'].map({'999':1,'5':0, '2':0})

corr = sql4.corr()

#sns.heatmap(sql4.corr(), annot=True, fmt=".2f")
#plt.show()

#%% Ouverture du nouveau dataframe

#df=pd.read_csv("export_commande_nuitducode_etat.csv")
#
## dropping null value columns to avoid errors 
#df.dropna(inplace = True) 
#   
## traitement des dates 
#df["date_vendu"]= df["date_vendu"].str.split(" ", n = 1, expand = True) 
#df = df.loc[df['date_insertion'] != '0000-00-00'] #on prend que les données ou la date n'est pas 000
#df = df.loc[df['date_vendu'] != '0000-00-00']
#df['date_insertion'] = pd.to_datetime(df.date_insertion) #conversion en date
#df['date_vendu'] = pd.to_datetime(df.date_vendu)
#df["days"] = (df["date_vendu"] - df["date_insertion"]) #Création d'une nouvelle colonne
#df['days'] = pd.to_numeric(df.days)




#sql3 = sql3.loc[sql3['etat']==1] # si on veut choisir juste un état
plt.figure(1)
plt.scatter(sql3["prix_ttc"],sql3["days"], c=sql3['etat'])
plt.xlabel('prix_ttc')
plt.ylabel('days')
#fonctionne mais je n'arrive pas à mettre les labels


sns.lmplot('prix_ttc','days', hue='etat',data=sql3, fit_reg=False)
#ici on voit les labels des point c'est good

sns.lmplot('prix_tot','days', hue='etat',data=sql3, fit_reg=False)
#même chose pour le prix total




#%%Affiche le délai de vente en fonction du prix pour l'état 4

sql3 = sql3.loc[sql3['etat']==4]
#sns.swarmplot(x="prix_ttc",y="days",data=sql3)
g = sns.factorplot("prix_tot", "days","etat", data=sql3, kind="point", palette="muted", legend=True) #Marche, days en fonction du prix total
plt.show()