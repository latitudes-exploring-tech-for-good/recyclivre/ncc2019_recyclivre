#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 16 11:22:41 2019

@author: camilledaveau
"""

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from convert_isbn_infos import convert_isbn_to_infos
from convert_book_vector import convert_book_to_vector
import csv


dataframe = pd.read_csv('/Users/camilledaveau/Desktop/ncc2019_recyclivre/data/export_commande_nuitducode_etat.csv')

 
dataframe.apply(convert_book_to_vector, axis=1)
 
        
    
print(dataframe.head())


#columns4 = ["etat", "statut","prix_ttc", "fdp_ttc", "date_mel","date_vendu" ]
#sql4 = sql2[columns4]
#
#sql4 = sql4.loc[sql4['date_mel'] != '0000-00-00'] #on prend que les données ou la date n'est pas 000
#sql4 = sql4.loc[sql4['date_vendu'] != '0000-00-00']
#sql4['date_mel'] = pd.to_datetime(sql4.date_mel) #conversion en date
#sql4['date_vendu'] = pd.to_datetime(sql4.date_vendu)
#sql4["days"] = (sql4["date_vendu"] - sql4["date_mel"]) #Création d'une nouvelle colonne
#sql4['days'] = pd.to_numeric(sql4.days)
#missing_val_count_by_column = (sql4.isnull().sum())
#print(missing_val_count_by_column[missing_val_count_by_column > 0])
#sql4['days'].value_counts()
#
### plot avec scatter prix en fonction du délai de vente et états
#sql4['days'] = pd.to_numeric(sql4.days) # je convertis days en float
#
#sql4["prix_tot"] = (sql4["prix_ttc"] + sql4["fdp_ttc"])
#
#del sql4['fdp_ttc']
#del sql4['prix_ttc']
#del sql4['date_mel']
#del sql4['date_vendu']
##sql4['statut']= sql4['statut'].map({'999':1,'5':0, '2':0})
#
#corr = sql4.corr()
#
#sns.heatmap(sql4.corr(), annot=True, fmt=".2f")
#plt.show()
