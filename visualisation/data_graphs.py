import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from numpy import *

import sys
#sys.path.append("..")
from utils.convert_isbn_infos import convert_isbn_to_infos

def price_wrt_time(isbn):
    """
    Input: ISBN-13 (String)
    Output: Graph showing price with respect to time for
            that ISBN
    """
<<<<<<< HEAD
    #csv_path = os.path.join("..", "data")
    raw_sql = pd.read_csv("C:/Users/charl/Documents/Centrale/Latitudes/ncc2019_recyclivre/data/export_commande_nuitducode_etat.csv")
=======

    raw_sql = pd.read_csv("/Users/camilledaveau/Desktop/ncc2019_recyclivre")

>>>>>>> dc8eae3896138c27b529f9da83349b0ebe9f9ba4

    # Choose columns that are relevant to the problem
    columns = ["asin", "date_vendu", "date_insertion", "etat", "prix_ttc", "fdp_ttc"]
    treated_sql = raw_sql[columns].copy(deep=True)

    # Filter out with isbn
    treated_sql = treated_sql.loc[treated_sql["asin"] == isbn]

    # Filter out 'year 0'
    treated_sql = treated_sql.loc[treated_sql["date_insertion"] != "0000-00-00"]
    treated_sql = treated_sql.loc[treated_sql["date_vendu"] != "0000-00-00 00:00:00"]

    treated_sql["date_insertion"] = pd.to_datetime(treated_sql.date_insertion)
    treated_sql["date_vendu"] = pd.to_datetime(treated_sql.date_vendu)
    
    days_stocked = (treated_sql["date_vendu"] - treated_sql["date_insertion"])
    days_stocked = pd.to_numeric(days_stocked)
    days_stocked /= (864 * 10 ** 11)
    treated_sql["jours_stock"] = days_stocked

    treated_sql["prix_ttc"] = raw_sql.prix_ttc
    treated_sql["fdp_ttc"] = raw_sql.fdp_ttc
    treated_sql["prix_total"] = treated_sql["prix_ttc"] + treated_sql["fdp_ttc"]


    plt.figure()
    plt.scatter(treated_sql["prix_total"], treated_sql["jours_stock"], c=treated_sql.etat)
    plt.xlabel('prix_ttc')
    plt.ylabel('jours_stock')
    #fonctionne mais je n'arrive pas à mettre les labels

    try:
        book_info = convert_isbn_to_infos(str(isbn))
        #Displays title of the book and its "main" author (first one)
        plt.title("\"" + book_info["Title"] + "\"" + ", par " + book_info["Authors"][0])
    except:
        plt.title("ISBN: " + isbn)

    sns.lmplot('prix_ttc', 'jours_stock', hue='etat',data=treated_sql, fit_reg=False)
    #ici on voit les labels des point c'est good

    sns.lmplot('prix_total','jours_stock', hue='etat',data=treated_sql, fit_reg=False)
    #même chose pour le prix total

    plt.show()


print(price_wrt_time('2213631301'))


def price_wrt_period(isbn):
    """
    Input: ISBN-13 (String)
    Output: Graph showing price with respect to the period of 
            the year for that ISBN
    """
    raw_sql = pd.read_csv("/Users/camilledaveau/Desktop/ncc2019_recyclivre/data/export_commande_nuitducode_etat.csv")

    # Choose columns that are relevant to the problem
    columns = ["asin", "date_vendu", "date_insertion", "etat", "prix_ttc", "fdp_ttc"]
    treated_sql = raw_sql[columns].copy(deep=True)

    # Filter out with isbn
    treated_sql = treated_sql.loc[treated_sql["asin"] == isbn]

    # Filter out 'year 0'
    treated_sql = treated_sql.loc[treated_sql["date_vendu"] != "0000-00-00 00:00:00"]

    treated_sql["date_vendu"] = pd.to_datetime(treated_sql.date_vendu)

    treated_sql["prix_ttc"] = raw_sql.prix_ttc
    treated_sql["fdp_ttc"] = raw_sql.fdp_ttc
    treated_sql["prix_total"] = treated_sql["prix_ttc"] + treated_sql["fdp_ttc"]


    plt.figure(1)
    plt.scatter(treated_sql["date_vendu"], treated_sql["prix_total"], c=treated_sql.etat)
    plt.plot(treated_sql["date_vendu"], treated_sql["prix_total"])
    plt.xlabel('jour_annee')
    plt.ylabel('prix_total')

    try:
        book_info = convert_isbn_to_infos(str(isbn))
        # Displays title of the book and its "main" author (first one)
        plt.title("\"" + book_info["Title"] + "\"" + ", par " + book_info["Authors"][0])
    except:
        plt.title("ISBN: " + isbn)
    #fonctionne mais je n'arrive pas à mettre les labels

    #sns.lmplot('prix_ttc', 'date_vendu', hue='etat', data=treated_sql, fit_reg=False)
    #ici on voit les labels des point c'est good

    #sns.lmplot('prix_total','date_vendu', hue='etat', data=treated_sql, fit_reg=False)
    #même chose pour le prix total

    plt.show()

price_wrt_period('2213631301')

def days_stocked_wrt_price(isbn):
    """
    Input: ISBN-13 (String)
    Output: Graph showing number of days spent in stock with respect 
            to the price of the book
    """

    raw_sql = pd.read_csv("/Users/camilledaveau/Desktop/ncc2019_recyclivre/data/export_commande_nuitducode_etat.csv")

    # Choose columns that are relevant to the problem
    columns = ["asin", "date_vendu", "date_insertion", "etat", "prix_ttc", "fdp_ttc"]
    treated_sql = raw_sql[columns].copy(deep=True)

    # Filter out with isbn
    treated_sql = treated_sql.loc[treated_sql["asin"] == isbn]

    # Filter out 'year 0'
    treated_sql = treated_sql.loc[treated_sql["date_insertion"] != "0000-00-00"]
    treated_sql = treated_sql.loc[treated_sql["date_vendu"] != "0000-00-00 00:00:00"]

    treated_sql["date_insertion"] = pd.to_datetime(treated_sql.date_insertion)
    treated_sql["date_vendu"] = pd.to_datetime(treated_sql.date_vendu)
    
    days_stocked = (treated_sql["date_vendu"] - treated_sql["date_insertion"])
    days_stocked = pd.to_numeric(days_stocked)
    days_stocked /= (864 * 10 ** 11)
    treated_sql["jours_stock"] = days_stocked

    treated_sql["prix_ttc"] = raw_sql.prix_ttc
    treated_sql["fdp_ttc"] = raw_sql.fdp_ttc
    treated_sql["prix_total"] = treated_sql["prix_ttc"] + treated_sql["fdp_ttc"]


    # Affiche le délai de vente en fonction du prix pour l'état 4
    plt.figure(1)
    try:
        book_info = convert_isbn_to_infos(str(isbn))
        # Displays title of the book and its "main" author (first one)
        plt.title("\"" + book_info["Title"] + "\"" + ", par " + book_info["Authors"][0])
    except:
        plt.title("ISBN: " + isbn)

    treated_sql = treated_sql.loc[treated_sql['etat'] == 4]
    sns.swarmplot(x="jours_stock", y="prix_ttc", data=treated_sql)
    sns.factorplot("jours_stock", "prix_total", "etat", data=treated_sql, kind="point", palette="muted", legend=True) 
   # plt.xticks(arange(min(treated_sql.jours_stock), max(treated_sql.jours_stock) + 1, (max(treated_sql.jours_stock) - min(treated_sql.jours_stock)) / 10))
    plt.show()


#days_stocked_wrt_price('2213631301')
#
#=======
#price_wrt_time('2213631301')
#>>>>>>> 93437b9b18da9b7110befca876fbc6c5a27f75bb
